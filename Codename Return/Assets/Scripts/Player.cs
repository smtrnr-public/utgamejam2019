﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    // instance vars

    public float moveSpeed;
    Animator anim;
    private bool has_item = true;
    private string prevKey = ""; // what was the last movement?
    private string prevStop = "movedAhead"; // what was the last stop?
    private string keyUp = "back";
    private string keyDown = "ahead";
    private string keyRight = "right";
    private string keyLeft = "left";

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.enabled = true;
        // make sure does not collide with shelves

        string hasItemOrNot = "player_";
        string thisKey = ""; // placeholder

        if (has_item)
            hasItemOrNot = "player_item_";

        // control player movement using arrow keys
        // arrow keys will not disrupt animation if the movement 
        // is the same from the prev frame
        //print(Input.anyKey + "<<><><>>" + prevKey + "-------" + prevStop);
        if (Input.anyKey) // check for direction change or keep moving?
        {
            // A key has been prompted by input
            if (Input.GetKey(KeyCode.UpArrow) && !prevKey.Equals(keyUp)) // move up
            {
                //transform.Translate(0, moveSpeed, 0); 
                thisKey = keyUp;
            }
            else if (Input.GetKey(KeyCode.DownArrow) && !prevKey.Equals(keyDown)) // move down
            {
                //transform.Translate(0, -moveSpeed, 0); 
                thisKey = keyDown;

            }
            else if (Input.GetKey(KeyCode.RightArrow) && !prevKey.Equals(keyRight)) // move right
            {
                //transform.Translate(moveSpeed, 0, 0); 
                thisKey = keyRight;

            }
            else if (Input.GetKey(KeyCode.LeftArrow) && !prevKey.Equals(keyLeft)) // move left
            {
                //transform.Translate(-moveSpeed, 0, 0); 
                thisKey = keyLeft;

            }

            // assume it plays the desired animation
            // if stop -> move: then make prevStop false
            // if move -> move: then stop moving in the previous direction
            // but if move -> move in the same direction, do nothing
            if (!prevKey.Equals(thisKey))
            {
                if (prevKey.Equals("")) // stop -> move
                {
                    //anim.enabled = false;
                    anim.Play(hasItemOrNot + "walk_" + thisKey);
                    // prevStop = "";
                }
                else // move -> move
                {
                    //anim.enabled = false;
                    anim.Play(hasItemOrNot + "walk_" + thisKey);
                }

                // there exists a 'thisKey'
                // print("expected there is a key: " + thisKey);
                
                prevKey = thisKey;
            }
        }
        else // stop moving and there is no previous key pressed
        {
            anim.enabled = false;
            anim.Play(hasItemOrNot + "idle_");
            prevKey = "";
        }

    }

    // pre: direction is prevKey
    // post: sets anim's moved<direction> boolean true and its <direction> boolean false
    // this allows to get a static frame where the player is oriented in the prev direction of movement
    string getIdlePos(string direction)
    {
        anim.SetBool(direction, false);
        string toReturn = "moved" + char.ToUpper(direction[0]) +
            direction.Substring(1);
        anim.SetBool(toReturn, true);
        return toReturn;

    }



}
