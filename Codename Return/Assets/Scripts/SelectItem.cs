﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectItem : MonoBehaviour
{
    public List<GameObject> stolenItems;
    public int currentIndex;
    public GameObject itemselected = null;
    public GameObject cam;
    public Image itemIcon;
    Game g;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = stolenItems[currentIndex].transform.position;
        g = FindObjectOfType<Game>();
        itemIcon.transform.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {   
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            currentIndex -= 1;
            if (currentIndex < 0 ) {
            currentIndex = stolenItems.Count-1;
            }
            while (stolenItems[currentIndex].activeSelf == false) {
            currentIndex -= 1;
            }
            transform.position = stolenItems[currentIndex].transform.position;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            currentIndex += 1;
            
            if (currentIndex > stolenItems.Count-1) {
            currentIndex = 0;
            }
            while (stolenItems[currentIndex].activeSelf == false) {
            currentIndex += 1;
            }
            transform.position = stolenItems[currentIndex].transform.position;
        }
        if(Input.GetKeyDown(KeyCode.Space)) {
            if (itemselected == null) {
                itemselected = stolenItems[currentIndex];
                SpriteRenderer sr = itemselected.GetComponent<SpriteRenderer>();
                g.SetItem(currentIndex);
                itemIcon.sprite = sr.sprite;
                itemIcon.transform.gameObject.SetActive(true);
            }
            itemselected.SetActive(false);
            StartCoroutine(waitTime(0.5f));
        }
        if (Input.GetKeyDown(KeyCode.B)) {
            StartCoroutine(waitTime(1));
        }
    }
        
    IEnumerator waitTime(float time) {
        yield return new WaitForSeconds(time);
        cam.SetActive(false);
    }
    public bool isEmpty() {
        foreach (GameObject item in stolenItems) {
            if (item.activeSelf == true) {
                return false;
            }
        }
        return true;
    }

}
