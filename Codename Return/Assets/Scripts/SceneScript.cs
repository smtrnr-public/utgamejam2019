﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneScript : MonoBehaviour
{
    public Transform SpawnPoint;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //Debug.Log("in");
            //LoadScene();
            other.transform.position = SpawnPoint.position;
            if (this.name == "ExitZone (1)")
            {
                susMeter.rate = 0;
            } else if (this.name == "ExitZone")
            {
                susMeter.rate = .1f;
            }

        }
    }
    public void LoadScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Apartment")
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }
}
