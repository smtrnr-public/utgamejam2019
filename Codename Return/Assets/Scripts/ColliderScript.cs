﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour
{
    Vector3 offset;
    void Start() {
        offset = transform.position - transform.parent.transform.position;

    }
    void Update()
    {
        transform.position = transform.parent.transform.position + offset;
    }
}
