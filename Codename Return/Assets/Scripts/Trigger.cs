﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Trigger : MonoBehaviour
{
    public GameObject popupText;
    public GameObject selectObject;
    public GameObject cam;
    private bool canInteract;
    void Start() {

    }
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            //Debug.Log("Enter");
            canInteract = true;
        }
    }
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player") {
            //Debug.Log("Exit");
            canInteract = false;
        }
    }
    void Update() {
        if (canInteract) {
            popupText.SetActive(true);
            if(Input.GetKeyDown(KeyCode.Space)) {
                if (selectObject.GetComponent<SelectItem>().isEmpty()) {
                    SceneManager.LoadScene(3);
                } else {
                    cam.SetActive(true);
                }
            }
        }
        else {
            popupText.SetActive(false);
        }
    }
}
