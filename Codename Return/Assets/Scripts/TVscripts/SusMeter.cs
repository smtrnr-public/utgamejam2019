﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SusMeter : MonoBehaviour
{
    private float TimeRemaining;
    private bool hasBeenSeen = true;
    [SerializeField]
    private float timerMax = 100f;
    public Slider slider;
    public bool start;

    void Start(){
        TimeRemaining = timerMax;
        start = true;
    }

    void Update()
    {
        if (start)
        {
            slider.value = timerCalculate();

        if (hasBeenSeen){
            //c.seen = false;
            hasBeenSeen = false;
        }
         if(TimeRemaining > 0){
            TimeRemaining -= (0.1f*Time.deltaTime);
            //Debug.Log(Time.time);
            //c.seen = false;
        }

            if (TimeRemaining < 0)
            {
                TimeRemaining = 0;

            }

            else if (TimeRemaining > 0)
            {
                TimeRemaining -= Time.deltaTime;
                //Debug.Log(Time.time);
            }
        }
    }

    float timerCalculate(){
        return (TimeRemaining / timerMax);
    }

    public void startTimer()
    {
        start = true;
    }

    public void stopTimer()
    {
        start = false;
    }

    public void resetTimer()
    {
        TimeRemaining = timerMax;
    }
}
