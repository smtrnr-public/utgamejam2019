﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class susMeter : MonoBehaviour
{
    Image SusMeter;
    float maxSus = 1000f;
    public static float sus;
    public static float rate;

    public Transform SpawnPoint;
    public GameObject player;
    public GameObject myText;
    // Start is called before the first frame update
    void Start()
    {
        myText.SetActive(false);
        rate = 0;
        SusMeter = this.GetComponent<Image> ();
        sus = 0;
    }

    // Update is called once per frame
    void Update()
    {
        sus += rate;
        SusMeter.fillAmount = sus / maxSus;
        if (sus >= maxSus)
        {
            player.transform.position = SpawnPoint.position;
            sus = 0;
            rate = 0;
            myText.SetActive(true);
            StartCoroutine(resetText(myText));
        }
    }

    public void startSus()
    {
        rate = .1f;
    }

    public void stopSus()
    {
        rate = 0;
    }

    public void resetSus()
    {
        sus = 0;
    }

    IEnumerator resetText (GameObject myText) {
        yield return new WaitForSeconds(3);
        myText.SetActive(false);
    }
}
