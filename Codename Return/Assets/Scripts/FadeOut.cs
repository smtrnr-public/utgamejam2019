﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{

void Update(){
        if(Input.GetKeyDown(KeyCode.Space)){
            FadeMe();
        }
    }

public void FadeMe() {
    StartCoroutine(DoFade());
}

IEnumerator DoFade(){
    CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
    while (canvasGroup.alpha<1){
        canvasGroup.alpha += Time.deltaTime /2;
        yield return null;
    }
    canvasGroup.interactable = false;
    yield return null;
}
}
