﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleScript : MonoBehaviour
{
    private SpriteRenderer sr;
    private Vector3 corner;
    public Transform circleObject;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.color = Color.green;
        //corner = new Vector3(transform.position.x - 0.5f, transform.position.y - 0.5f, transform.position.z);
        corner = circleObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        corner = circleObject.transform.position;
        transform.RotateAround(corner, new Vector3(0,0,.5f), Mathf.Sin(Time.time * 2f));
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            sr.color = Color.yellow;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            sr.color = Color.white;
        }
    }
}
