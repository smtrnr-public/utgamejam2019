﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShelfHitBox : MonoBehaviour
{
    private bool inRange;
    public Image itemIcon;
    public GameObject item;
    SpriteRenderer sr;
    public GameObject selectItem;
    SelectItem si;
    // Start is called before the first frame update
    void Start()
    {
        item.SetActive(false);
        sr = item.GetComponent<SpriteRenderer>();
        si = selectItem.GetComponent<SelectItem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inRange && Input.GetKeyDown(KeyCode.Space))
        {
            item.SetActive(true);
            itemIcon.gameObject.SetActive(false);
            si.itemselected = null;

            //Debug.Log("space");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (sr.sprite == itemIcon.sprite) {
                inRange = true;
            }
            //Debug.Log("in");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            inRange = false;
            //Debug.Log("out");
        }
    }
}
