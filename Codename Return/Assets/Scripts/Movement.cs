﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public GameObject shelfCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (shelfCamera.activeSelf == false) {
            Vector3 horizontal = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            Vector3 vertical = new Vector3(0, 0, Input.GetAxis("Vertical"));
            transform.position = transform.position + (horizontal + vertical) * Time.deltaTime * moveSpeed;

        }
        
        /*if (Input.GetKey(KeyCode.UpArrow)) // move up
            {
                transform.Translate(0, moveSpeed*Time.deltaTime, 0); 
            }
        else if (Input.GetKey(KeyCode.DownArrow)) // move down
        {
            transform.Translate(0, -moveSpeed*Time.deltaTime, 0);

        }
        else if (Input.GetKey(KeyCode.RightArrow)) // move right
        {
            transform.Translate(moveSpeed*Time.deltaTime, 0, 0);

        }
        else if (Input.GetKey(KeyCode.LeftArrow)) // move left
        {
            transform.Translate(-moveSpeed*Time.deltaTime, 0, 0);

        }*/
    }
}
