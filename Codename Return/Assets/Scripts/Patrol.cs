﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public Transform[] waypoint;
    public float speed;
    public int currentWaypoint = 1;
    Rigidbody2D rb;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        transform.position = waypoint[0].position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, waypoint[currentWaypoint].position) < 0.2f) {
            currentWaypoint += 1;           
           
            if (currentWaypoint > waypoint.Length-1){ 
                currentWaypoint = 0;
                
            }
            Vector3 dir = (this.transform.position - waypoint[currentWaypoint].transform.position).normalized;
            if (dir.x < -0.2) {
                anim.Play("Employee_walkRight");
                //Debug.Log(dir +"Right");
            }
            if (dir.x > 0.2) {
                anim.Play("Employee_walkLeft");
                //Debug.Log(dir +"Left");
            }
            if (dir.z < -0.2) {
                anim.Play("Employee_walkUp");
                //Debug.Log(dir +"Up");
            }
            if (dir.z > 0.2) {
                anim.Play("Employee_walkDown");
                //Debug.Log(dir +"Down");
            }
        }
        else {
            transform.position = Vector3.MoveTowards(transform.position, waypoint[currentWaypoint].position, speed*Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            //Debug.Log(rb.velocity);
        }
    }
}
