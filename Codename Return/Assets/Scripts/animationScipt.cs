﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationScipt : MonoBehaviour
{
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {   
        anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
        
        if (Input.GetKey(KeyCode.UpArrow)) // move up
        {
            
        }
        else if (Input.GetKey(KeyCode.DownArrow)) // move down
        {
            anim.Play("player_item_walk_ahead");
        }
        else if (Input.GetKey(KeyCode.RightArrow)) // move right
        {
            

        }
        else if (Input.GetKey(KeyCode.LeftArrow)) // move left
        {
            

        }
    }
}
